import React, { Component, Fragment } from 'react';

import AddformSumExpenses from './component/AddFormSumExpenses/AddFormSumExpenses'
import Task from './component/Task/Task'

import './App.css';

class App extends Component {
  state = {
      value: '',
      string: '',
      tasks : [],
      totalPrice: 0
  };

   addTaskHandler = () => {
       let newTask = {task: this.state.value, cost: this.state.string};
       let tasks = [...this.state.tasks, newTask];

       const oldPrice = this.state.totalPrice;

       const priceAddition = parseInt(this.state.string);

       const newPrice = oldPrice  + priceAddition;

       this.setState({tasks, value: '', totalPrice: newPrice})
   };

    removedItem = (value, cost) => {
        let tasks = [...this.state.tasks];
        const oldPrice = this.state.totalPrice;

        const newPrice = oldPrice - parseInt(cost);

        const index = tasks.findIndex((item) => item.task === value);
        tasks.splice(index, 1);

        this.setState({
            tasks: tasks, totalPrice: newPrice
        });
    };


    inputChange = (string) => (
        this.setState({string})
    );

    inputChangeHandler = (value) => (
        this.setState({value})
    );
  render() {
    return (
        <Fragment>
     <AddformSumExpenses
         value={this.state.value}
         val={this.state.string}
         changed={this.inputChangeHandler}
         valChanged={this.inputChange}
         addTask={this.addTaskHandler}

     />
          <div className="block-app">
              {this.state.tasks.map((task, id) => <Task task={task}  key={id} removed={(value, cost) => this.removedItem(value, cost)}/>)}
              <div className="center-text"><p>Total spent {this.state.totalPrice} KGS</p></div>
          </div>


        </Fragment>
    );
  }
}

export default App;