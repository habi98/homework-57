import React from 'react'

import './AddFormSumExpenses.css'

const AddformSumExpenses = props => {
   return(
       <div>
           <input type="text" className="name" value={props.value} onChange={(e) => props.changed(e.target.value)}/>
           <input type="number" className="sum"  val={props.string} onChange={(event) => props.valChanged(event.target.value)} />
           <label>kgs</label>
           <button type="button" className="btn-add" onClick={props.addTask}>add</button>
       </div>

   )
};

export default AddformSumExpenses;