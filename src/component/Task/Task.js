import React from 'react';


const Task = (props) => {
    return (
        <div>
            <p>{props.task.task}</p>
            <p>потрачено: {props.task.cost} <button onClick={() => props.removed(props.task.task, props.task.cost)}>x</button></p>
        </div>
    )
};

export default Task;